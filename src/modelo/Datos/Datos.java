/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.Datos;

/**
 *
 * @author LENOVO
 */
public class Datos {
    private String cadenaCaracter;
    private byte bit;
    private short corto;
    private int entero;
    private long largo;
    private float flotante;
    private double doble;
    private char caracter;
    private Long memoria;

    public byte getBit() {
        return bit;
    }

    public void setBit(byte bit) {
        this.bit = bit;
    }

    public short getCorto() {
        return corto;
    }

    public void setCorto(short corto) {
        this.corto = corto;
    }

    public int getEntero() {
        return entero;
    }

    public void setEntero(int entero) {
        this.entero = entero;
    }

    public long getLargo() {
        return largo;
    }

    public void setLargo(long largo) {
        this.largo = largo;
    }

    public float getFlotante() {
        return flotante;
    }

    public void setFlotante(float flotante) {
        this.flotante = flotante;
    }

    public double getDoble() {
        return doble;
    }

    public void setDoble(double doble) {
        this.doble = doble;
    }

    public char getCaracter() {
        return caracter;
    }

    public void setCaracter(char caracter) {
        this.caracter = caracter;
    }

    public String getCadenaCaracter() {
        return cadenaCaracter;
    }

    public void setCadenaCaracter(String cadenaCaracter) {
        this.cadenaCaracter = cadenaCaracter;
    }

    public Long getMemoria() {
        return memoria;
    }

    public void setMemoria(Long memoria) {
        this.memoria = memoria;
    }
}
