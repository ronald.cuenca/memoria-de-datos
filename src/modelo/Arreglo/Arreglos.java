/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.Arreglo;

/**
 *
 * @author LENOVO
 */
public class Arreglos {
    private String[][] arregloString;
    private int[][] arregloEntero;
    private char[][] arregloCaracter;
    private double[][] arregloDouble;
    private byte[][] arregloByte;

    public String[][] getArregloString() {
        return arregloString;
    }

    public void setArregloString(String[][] arregloString) {
        this.arregloString = arregloString;
    }

    public int[][] getArregloEntero() {
        return arregloEntero;
    }

    public void setArregloEntero(int[][] arregloEntero) {
        this.arregloEntero = arregloEntero;
    }

    public char[][] getArregloCaracter() {
        return arregloCaracter;
    }

    public void setArregloCaracter(char[][] arregloCaracter) {
        this.arregloCaracter = arregloCaracter;
    }

    public double[][] getArregloDouble() {
        return arregloDouble;
    }

    public void setArregloDouble(double[][] arregloDouble) {
        this.arregloDouble = arregloDouble;
    }

    public byte[][] getArregloByte() {
        return arregloByte;
    }

    public void setArregloByte(byte[][] arregloByte) {
        this.arregloByte = arregloByte;
    }
}
