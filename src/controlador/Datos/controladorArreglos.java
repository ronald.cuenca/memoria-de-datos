/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Datos;

import modelo.Arreglo.Arreglos;
import org.github.jamm.MemoryMeter;

/**
 *
 * @author LENOVO
 */
public class controladorArreglos {

    Arreglos arreglo;

    public controladorArreglos() {
        this.arreglo = null;
    }

    public Arreglos getArreglo() {
        return arreglo;
    }

    public void setArreglo(Arreglos arreglo) {
        this.arreglo = arreglo;
    }

    public Long insertarString(String s, Integer Fila, Integer Columna) {
        MemoryMeter meter = MemoryMeter.builder().build();
        Arreglos arregloAux = new Arreglos();
        String[][] s2 = new String[Fila][Columna];
        if (s.equals("")) {
            arregloAux.setArregloString(s2);
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloString());
        } else {
            for (int i = 0; i < Fila; i++) {
                for (int j = 0; j < Columna; j++) {
                    s2[i][j] = s;
                    arregloAux.setArregloString(s2);
                }
            }
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloString());
        }
    }

    public Long insertarEntero(int in, Integer Fila, Integer Columna) {
        MemoryMeter meter = MemoryMeter.builder().build();
        Arreglos arregloAux = new Arreglos();
        int[][] in2 = new int[Fila][Columna];
        if (in == 0) {
            arregloAux.setArregloEntero(in2);
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloEntero());
        } else {
            for (int i = 0; i < Fila; i++) {
                for (int j = 0; j < Columna; j++) {
                    in2[i][j] = in;
                    arregloAux.setArregloEntero(in2);
                }
            }
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloEntero());
        }
    }

    public Long insertarCaracter(char c, Integer Fila, Integer Columna) {
        Arreglos arregloAux = new Arreglos();
        char[][] c2 = new char[Fila][Columna];
        for (int i = 0; i < Fila; i++) {
            for (int j = 0; j < Columna; j++) {
                c2[i][j] = c;
                arregloAux.setArregloCaracter(c2);
            }
        }
        arreglo = arregloAux;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(arreglo.getArregloCaracter());
    }

    public Long insertarDouble(double d, Integer Fila, Integer Columna) {
        MemoryMeter meter = MemoryMeter.builder().build();
        Arreglos arregloAux = new Arreglos();
        double[][] d2 = new double[Fila][Columna];
        if (d == 0 || d == 0.0) {
            arregloAux.setArregloDouble(d2);
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloDouble());
        } else {
            for (int i = 0; i < Fila; i++) {
                for (int j = 0; j < Columna; j++) {
                    d2[i][j] = d;
                    arregloAux.setArregloDouble(d2);
                }
            }
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloDouble());
        }
    }

    public Long insertarBit(byte b, Integer Fila, Integer Columna) {
        MemoryMeter meter = MemoryMeter.builder().build();
        Arreglos arregloAux = new Arreglos();
        byte[][] b2 = new byte[Fila][Columna];
        if (b == 0) {
            arregloAux.setArregloByte(b2);
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloByte());
        } else {
            for (int i = 0; i < Fila; i++) {
                for (int j = 0; j < Columna; j++) {
                    b2[i][j] = b;
                    arregloAux.setArregloByte(b2);
                }
            }
            arreglo = arregloAux;
            return meter.measureDeep(arreglo.getArregloByte());
        }
    }

    public Arreglos obtenerDato() {
        return arreglo;
    }
}
