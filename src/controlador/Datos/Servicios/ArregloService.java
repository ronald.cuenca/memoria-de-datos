/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Datos.Servicios;

import controlador.Datos.controladorArreglos;
import modelo.Arreglo.Arreglos;

/**
 *
 * @author LENOVO
 */
public class ArregloService {
    controladorArreglos arregloService = new controladorArreglos();

    public controladorArreglos getArregloService() {
        return arregloService;
    }

    public void setArregloService(controladorArreglos arregloService) {
        this.arregloService = arregloService;
    }
    
    public Long memoriaString(String s, Integer Fila, Integer Columna){
        return arregloService.insertarString(s, Fila, Columna);
    }
    
    public Long memoriaEntero(int i, Integer Fila, Integer Columna){
        return arregloService.insertarEntero(i, Fila, Columna);
    }
    
    public Long memoriaCaracter(char c, Integer Fila, Integer Columna){
        return arregloService.insertarCaracter(c, Fila, Columna);
    }
    
    public Long memoriaDouble(double d, Integer Fila, Integer Columna){
        return arregloService.insertarDouble(d, Fila, Columna);
    }
    
    public Long memoriaBit(byte b, Integer Fila, Integer Columna){
        return arregloService.insertarBit(b, Fila, Columna);
    }
    
    public Arreglos obtenerDato(){
        return arregloService.obtenerDato();
    }
}
