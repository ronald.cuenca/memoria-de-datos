/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Datos.Servicios;

import controlador.Datos.controladorDatos;
import modelo.Datos.Datos;
import org.github.jamm.MemoryMeter;

/**
 *
 * @author LENOVO
 */
public class DatosService {

    private controladorDatos datosService=new controladorDatos();

    public controladorDatos getDatosService() {
        return datosService;
    }

    public void setDatosService(controladorDatos datosService) {
        this.datosService = datosService;
    }

    public long memoriaString(String s) {
        return datosService.insertarString(s);
    }
    
    public long memoriaChar(char c) {
        return datosService.insertarChar(c);
    }
    
    public long memoriaInt(Integer i) {
        return datosService.insertarInt(i);
    }
    
    public long memoriaShort(short sh) {
        return datosService.insertarShort(sh);
    }
    
    public long memoriaLong(Long l) {
        return datosService.insertarLong(l);
    }
    
    public long memoriaFloat(float f) {
        return datosService.insertarFloat(f);
    }
    
    public long memoriaDouble(double d) {
        return datosService.insertarDouble(d);
    }
    
    public long memoriaByte(byte b) {
        return datosService.insertarByte(b);
    }
    
    public Datos obtenerDato(){
        return datosService.obtenerDato();
    }
  
}
