/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Datos.Servicios;

import controlador.Datos.controladorLista;
import modelo.Lista.Lista;

/**
 *
 * @author LENOVO
 */
public class ListaService {
    controladorLista listaService = new controladorLista();

    public controladorLista getListaService() {
        return listaService;
    }

    public void setListaService(controladorLista listaService) {
        this.listaService = listaService;
    }
    
    public Long memoriaLista(Object o){
        return listaService.memoriaLista(o);
    }
    
    public Lista obtenerDato(){
        return listaService.obtenerDato();
    }
}
