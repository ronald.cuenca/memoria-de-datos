/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Datos.Servicios;

import controlador.Datos.controladorEnlazada;

/**
 *
 * @author LENOVO
 */
public class EnlazadaService <E>{
    controladorEnlazada enlazadaService = new controladorEnlazada();

    public controladorEnlazada getEnlazadaService() {
        return enlazadaService;
    }

    public void setEnlazadaService(controladorEnlazada enlazadaService) {
        this.enlazadaService = enlazadaService;
    }
    
    public Long memoriaEnlazada(E dato){
        return enlazadaService.memoriaEnlazada(dato);
    }
    
    public E obtenerDato(){
        return (E) enlazadaService.obtenerDato();
    }
}
