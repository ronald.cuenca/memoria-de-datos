/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Datos;

import java.util.ArrayList;
import java.util.List;
import modelo.Lista.Lista;
import org.github.jamm.MemoryMeter;

/**
 *
 * @author LENOVO
 */
public class controladorLista {
    Lista lista;

    public controladorLista() {
        lista = null;
    }

    public Lista getLista() {
        return lista;
    }

    public void setLista(Lista lista) {
        this.lista = lista;
    }
    
    public Long memoriaLista(Object o){
        MemoryMeter meter = MemoryMeter.builder().build();
        Lista listaObject = new Lista();
        List<Object> aux = new ArrayList<Object>();
        if (o.equals(null)) {
            listaObject.setListaObjetos(aux);
            lista = listaObject;
            return meter.measureDeep(lista.getListaObjetos()); 
        }else{
            for (int i = 0; i < 25; i++) {
                aux.add(o);
                listaObject.setListaObjetos(aux);
            }
            lista = listaObject;
        return meter.measureDeep(lista.getListaObjetos()); 
        }
    }
    
    public Lista obtenerDato(){
        return lista;
    }
}
