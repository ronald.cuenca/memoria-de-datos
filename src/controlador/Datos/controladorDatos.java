/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Datos;

import java.util.Scanner;
import modelo.Datos.Datos;
import org.github.jamm.MemoryMeter;
import static org.github.jamm.MemoryMeter.builder;

/**
 *
 * @author LENOVO
 */
public class controladorDatos {

    private Datos tipoDato;

    public controladorDatos() {
        this.tipoDato = null;
    }

    public Datos getDatos() {
        return tipoDato;
    }

    public void setDatos(Datos tipoDatos) {
        this.tipoDato = tipoDatos;
    }

    public Long insertarString(String s) {
        Datos dato = new Datos();
        dato.setCadenaCaracter(s);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getCadenaCaracter());
    }
    
    public Long insertarChar(char c) {
        Datos dato = new Datos();
        dato.setCaracter(c);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getCaracter());
    }
    
    public Long insertarInt(Integer i) {
        Datos dato = new Datos();
        dato.setEntero(i);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getEntero());
    }
    
    public Long insertarShort(short sh) {
        Datos dato = new Datos();
        dato.setCorto(sh);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getCorto());
    }
    
    public Long insertarLong(Long l) {
        Datos dato = new Datos();
        dato.setLargo(l);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getLargo());
    }
    
    public Long insertarFloat(Float f) {
        Datos dato = new Datos();
        dato.setFlotante(f);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getFlotante());
    }
    
    public Long insertarDouble(Double d) {
        Datos dato = new Datos();
        dato.setDoble(d);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getDoble());
    }
    
    public Long insertarByte(byte b) {
        Datos dato = new Datos();
        dato.setBit(b);
        tipoDato = dato;
        MemoryMeter meter = MemoryMeter.builder().build();
        return meter.measureDeep(tipoDato.getBit());
    }
    
    public Datos obtenerDato(){
        return tipoDato;
    } 
}
