package controlador.Datos;

import modelo.Lista.NodoLista;
import org.github.jamm.MemoryMeter;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author LENOVO
 */
public class controladorEnlazada<E> {

    private NodoLista<E> cabecera;

    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabecera) {
        this.cabecera = cabecera;
    }

    public controladorEnlazada() {
        cabecera = null;
    }

    public Boolean estaVacia() {
        return cabecera == null;
    }

    public Long memoriaEnlazada(E dato) {
        MemoryMeter meter = MemoryMeter.builder().build();
        NodoLista<E> nuevo = new NodoLista<>(dato, null);
        if (estaVacia() || dato == null) {
            cabecera = nuevo;
            return meter.measureDeep(cabecera);
        } else {
            for (int i = 0; i < 25; i++) {
            nuevo.setSiguiente(cabecera);
            cabecera = nuevo;
            }
            return meter.measureDeep(nuevo);
        }
    }

    public E obtenerDato() {
        E dato;
        if (estaVacia()) {
            dato = null;
        } else {
            NodoLista<E> aux = cabecera;
            dato = aux.getDato();
        }
        return dato;
    }
}
